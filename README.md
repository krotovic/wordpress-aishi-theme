# Aishi Theme

- Authors: [@krotovic][github-krotovic]
- License: MIT
- Link: [http://wordpress.krotovic.cz](#) _(not available yet)_
- Tags: anime, manga, aishi, aishiteam, html5, css3, responsive
- Stable tag: 1.0

## Description

This is theme created especially for site of Aishi Team and is based upon Wordpress' theme TwentyTen.

## Installation

You can install this theme into your Wordpress site simply by searching on Wordpress' theme repository for keyword "aishi". _(currently not available)_
Or just [download][download] this repository and unzip it in
```bash
$ /WORDPRESS_ROOT/wp-content/themes/
```
This theme doesn't require anything because it's not a child theme but complete custom theme with anime/manga style.

## Frequently Asked Questions

### Can I use and edit this theme?

Yes. This theme is licensed under MIT license. Please, for more information, read about MIT license on [Wikipedia][mit-license].

## Screenshots

//TODO: Insert complete site screenshot//

-----
## Changelog

#### 1.0
* Initial release

----------------------

This theme is made for [WordPress](http://wordpress.org/ "Your favorite software") and this file is written in [Markdown][markdown syntax].

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown's Documentation"

[twentyten]: https://wordpress.org/themes/twentyten
[github-krotovic]: https://github.com/krotovic
[mit-license]: http://en.wikipedia.org/wiki/MIT_License
[download]: https://github.com/krotovic/wordpress-aishi-theme/archive/master.zip