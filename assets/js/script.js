/*
 * The MIT License
 *
 * Copyright 2014 Lukáš Krotovič <lukas@krotovic.cz>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

 function _ready (callback) {
    document.addEventListener("DOMContentLoaded", function(){
        callback();
    }, false);
 }

var App = (function (_$) {

    function App() {}

    App.prototype.Modules = {
        Search : function(_e){
            var _a = _e.children("a.search-btn");
            var _b = _e.children("div.search-wrapper");
            if (_a) {
                _$(_a).on("click", function () {
                    _$(_b).toggle();
                });
                _$(_b).hide();
            }
        }
    };
    App.prototype.init = function () {
        //this.Modules.search(_$("#search"));
    };

    App.prototype._reload = function () {
        location.reload();
    }

    App.Modules.Slideshow = function (container) {
        function _get_slide (slide) {
            if (inCache(slide)) return getSlideFromCache(slide);
            else {
                var d;
                _$.ajax({
                    method: "GET",
                    url: location.origin + "/wp-content/themes/aishitheme/assets/img/slideshow/" + slide.src
                }).
                success(function (data){
                    d = data;
                }).
                error(function (data){
                    console.error("[App] Slideshow: An error occured while getting slide " + slide.toString() + ".", data);
                });
            }
        }

        if (typeof container !== "undefined") {

        }
    }

    return new App();
})(jQuery);

window._reload = App._reload;
_ready(function () {
    App.init();
});