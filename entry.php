<article class="post" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <section class="post-header-wrapper" style="background-image: url('<?php
        echo (has_post_thumbnail())?
            wp_get_attachment_url(get_post_thumbnail_id()) :
            get_template_directory_uri().'/assets/img/default_thumbnail.png';
        ?>');">
            <h1 class="entry-title">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
                    <?php the_title(); ?>
                </a>
            </h1>
            <?php
            edit_post_link();
            get_template_part('entry', 'meta');
            ?>
        </section>

    <?php
    get_template_part('entry', ( (is_archive() || is_search() || is_home() ) ? 'summary' : 'content'));
    get_template_part('entry', 'footer');
    ?>
</article>