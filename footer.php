<div class="clear"></div>
</div>
<footer id="footer" class="full-width" role="contentinfo">
    <div id="copyright" class="">
        <?php echo sprintf(__('%1$s %2$s %3$s. All Rights Reserved.', 'aishitheme'), '&copy;', date('Y'), esc_html(get_bloginfo('name')));
        echo "<br/>" . sprintf(__(' Theme By: %1$s.', 'aishitheme'), 'Lukas Krotovic (based on <a href="http://tidythemes.com/">BlankSlate</a>)');
        ?>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>