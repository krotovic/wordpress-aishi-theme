<?php

function aishitheme_init() {
    load_theme_textdomain('aishitheme', get_template_directory() . '/languages');
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    $args = array(
        "random-default" => true
    );
    add_theme_support("custom-header", $args);
    register_nav_menus(array(
        "aishi-logged-in-menu" => "Menu for Logged in Users",
        "aishi-logged-out-menu" => "Menu for Guests"
    ));
}

function aishitheme_load_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('aishitheme_script', get_stylesheet_directory_uri() . '/assets/js/script.js');
    wp_enqueue_style('aishitheme_style', get_stylesheet_directory_uri() . '/assets/css/style.css');
}

function aishitheme_enqueue_comment_reply_script() {
    if (get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

function aishitheme_title($title) {
    if ($title == '') {
        return '&rarr;';
    } else {
        return $title;
    }
}

function aishitheme_filter_wp_title($title) {
    return $title . esc_attr(get_bloginfo('name'));
}

function aishitheme_widgets_init() {
    register_sidebar(array(
        'name' => __('AishiTeam Partners', 'aishitheme'),
        'id' => 'aishi-widgets-partners',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => "</li>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
       /**
        * Creates a sidebar
        * @param string|array  Builds Sidebar based off of 'name' and 'id' values.
        */
        $args = array(
            'name'          => __( 'Main sidebar', 'aishitheme' ),
            'id'            => 'aishi-widgets-main',
            'description'   => 'Default sidebar area',
            'class'         => 'aishi-widget',
            'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
            'after_widget'  => '</li>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>'
        );
    
        register_sidebar( $args );
    
}

function aishitheme_custom_pings($comment) {
    $GLOBALS['comment'] = $comment;
    ?>
        <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
    <?php
}

function aishitheme_comments_number($count) {
    if (!is_admin()) {
        global $id;
        $comments_by_type = &separate_comments(get_comments('status=approve&post_id=' . $id));
        return count($comments_by_type['comment']);
    } else {
        return $count;
    }
}

function aishitheme_custom_post_type() {
    register_post_type("aishi_slideshow", array(
        "labels" => array(
            "name" => __("Slideshow Items"),
            "singular_name" => __("Slideshow Item"),
            "menu_name" => __("Slideshow"),
            "add_new" => __("Add Slide"),
            "add_new_item" => __("Add New Slide"),
            "edit_item" => __("Edit Slide"),
            "new_item" => __("New Slide"),
            "view_item" => __("View Slide"),
            "search_items" => __("Search Slides"),
            "not_found" => __("No slides found"),
            "not_found_in_trash" => __("No slides found in Trash")
        ),
        "description" => "Slideshow in header",
        "public" => true,
        "publicly_queryable" => false,
        "show_ui" => true,
        "menu_position" => 5,
        "hierarchical" => false,
        "supports" => array(
            "title", "editor", "author", "thumbnail", "revisions"
        )
    ));
}

function five_posts_on_homepage( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'posts_per_page', 7 );
    }
}
add_action( 'pre_get_posts', 'five_posts_on_homepage' );


add_action('after_setup_theme', 'aishitheme_init');
add_action('comment_form_before', 'aishitheme_enqueue_comment_reply_script');
add_action("init", "aishitheme_custom_post_type");
add_filter('get_comments_number', 'aishitheme_comments_number');
add_action('widgets_init', 'aishitheme_widgets_init');
add_filter('the_title', 'aishitheme_title');
add_action('wp_enqueue_scripts', 'aishitheme_load_scripts');
add_filter('wp_title', 'aishitheme_filter_wp_title');
