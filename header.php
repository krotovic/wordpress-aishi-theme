<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width" />
        <title><?php wp_title(' | ', true, 'right'); ?></title>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class();
        echo get_template(); ?>>
        
        <div id="wrapper" class="hfeed">
            <header id="header" role="banner">
                <section id="branding" style="background: url(<?php echo (get_header_image() != "")? get_header_image() : get_template_directory_uri()."/assets/img/default_thumbnail.png"; ?>); background-size: cover;">
                    <div id="site-title">
                        <h1><a href="<?php echo esc_url(home_url('/')); ?>" title="<?php esc_attr_e(get_bloginfo('name'), 'aishitheme'); ?>" rel="home"><?php echo esc_html(get_bloginfo('name')); ?></a></h1>
                    </div>
                    <div id="site-description"><?php bloginfo('description'); ?></div>
                </section>
                <nav id="menu" role="navigation">
                    <?php
                    wp_nav_menu(array("theme_location" => "primary"));
                    ?>
                    <?php get_search_form(); ?>
                    <ul id="user-menu" class="user-menu-wrapper">
                        <li>
                            <a href="#" class="user-btn"><i class="icon-user"></i></a>
                        </li>
                        <li class="submenu init-hide">
                            <?php 
                            if (is_user_logged_in()) {
                                wp_nav_menu(array('theme_location' => 'aishi-logged-in-menu'));
                            } else {
                                wp_nav_menu(array('theme_location' => 'aishi-logged-out-menu'));
                            }
                            ?>
                        </li>
                    </ul>
                </nav>
            </header>
            <?php
            $args = array("post_type" => "aishi_slideshow", "posts_per_page" => 5);
            $slides = new WP_Query($args);
            if (is_home() && $slides->have_posts()):
                ?>
                <div id="slideshow">
                        <?php
                        $slide_id = array();
                        while ($slides->have_posts()):
        $slides->the_post();
        $slide_id[] = get_the_ID();
                            get_template_part("slideshow");
                endwhile;
                ?>
                    <div class="controls">
                            <?php
                             foreach ($slide_id as $id) {
                                ?>
                        <span class="icon-point" data-slide="<?php echo $id; ?>"></span>
                                <?php
                                    }
                             ?>
                            </div>
                    </div>
                <?php
            endif;
            ?>
            <div id="container">