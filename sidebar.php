<aside id="sidebar" role="complementary">
        <?php if (is_active_sidebar('aishi-widgets-partners')) : ?>
        <ul id="aishi-widgets-partners">
                <?php dynamic_sidebar('aishi-widgets-partners'); ?>
            </ul>
    <?php endif; ?>
</aside>