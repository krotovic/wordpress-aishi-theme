<div class="item slideshow" id="slide-<?php the_ID(); ?>" >
    <div class="img-wrapper"><?php the_post_thumbnail(array(600, 800)); ?></div>
    <div class="content-wrapper">
    	<h1>
        	<?php the_title(); ?>
        	<small><?php the_author(); ?></small>
    	</h1>
    	<div class="content"><?php the_content(); ?></div>
    </div>
</div>